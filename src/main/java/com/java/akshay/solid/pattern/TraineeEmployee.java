package com.java.akshay.solid.pattern;

public class TraineeEmployee extends Employee {

	private String location;

	public TraineeEmployee(String empId, String empName, Address address, String location) {
		super(empId, empName, address);
		this.location = location;
	}

	@Override
	public String toString() {
		return "TraineeEmployee [location=" + location + "]";
	}
	
	

}
