package com.java.akshay.solid.pattern;

class Penguin extends Bird {
    @Override
    void fly() {
        System.out.println("Penguin cannot fly");
    }

    void swim() {
        System.out.println("Penguin is swimming");
    }
}
