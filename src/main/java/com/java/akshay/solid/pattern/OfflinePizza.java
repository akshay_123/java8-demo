package com.java.akshay.solid.pattern;

public interface OfflinePizza {
	void acceptOfflinePayment();

	void collectFromStore();

}
