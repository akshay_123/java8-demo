package com.java.akshay.solid.pattern;

public class OCPHandler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Address address = new Address("addr1", "addr2", "addr3");

		// we dont'need to modify original Employee object instaed we can exted it and
		// add additional location to it. so wherever we have created emp object which
		// we dont' need to be modifu
		TraineeEmployee trainee = new TraineeEmployee("1", "empName", address, "Pune");
		System.out.println(trainee);

	}

}
