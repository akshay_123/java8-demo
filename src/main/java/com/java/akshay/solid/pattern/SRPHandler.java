package com.java.akshay.solid.pattern;

public class SRPHandler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Address address = new Address("addr1", "addr2", "addr3");

		// If there is any any changes in Address then we don't need to changes Employee
		// object
		Employee employee = new Employee("1", "Akshay", address);
		System.out.println(employee);
	}

}
