package com.java.akshay.solid.pattern;

public interface Pizza {

	void acceptOnlinePayment();

	void acceptOfflinePayment();

	void collectFromStore();

	void onlineDelivery();

}
