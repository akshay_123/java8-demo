package com.java.akshay.solid.pattern;

public class LSPHandler {

	public static void main(String[] args) {
		// here we can independently set the width and height
		Rectangle rectangle1 = new Rectangle(10, 20);
		System.out.println("Area: " + rectangle1.calculateArea());

		// LSP violated because the typical behavior of a rectangle (where you can
		// independently set the width and height) is altered by the Square subclass
		// (where setting one dimension automatically sets the other to the same value).
		Rectangle rectangle = new Square(5);
		rectangle.setWidth(10);
		rectangle.setHeight(20);

		//Expected 200 but in acatual getting 400
		System.out.println("Area: " + rectangle.calculateArea());

	}

}
