package com.java.akshay;

import java.util.Arrays;

class A
{
    int i=10;
}
 
class B extends A
{

    int i = 20;
}
 
public class MainClass
{
    public static void main(String[] args)
    {
        A a = new B();
        System.out.println(a.i);
    	char[] s1="RAT".toCharArray();
    	char[] s2="TAR".toCharArray();
    	
    	if(checkAnagram(s1,s2)) {
    		System.out.println("Anagram");
    	} else {
			System.out.println("not an angram");
		}

    }
    
    public static boolean checkAnagram(char[] s1,char[] s2) {
    	if(s1.length !=s2.length) {
    		return false;
    	}
    	Arrays.sort(s1);
    	Arrays.sort(s2);
    	for(int i=0;i<s1.length;i++) {
    		if(s1[i]!=s2[i]) {
    			return false;
    		}
    	}
    	return true;
    }
}
