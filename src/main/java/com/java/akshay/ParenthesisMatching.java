package com.java.akshay;

import java.util.Stack;

public class ParenthesisMatching {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = "(({{}}[]))";
		boolean result = isBrancketValid(input);
		System.out.println(result);

	}

	private static boolean isBrancketValid(String input) {
		try {
			Stack<Character> stack = new Stack<>();
			for (int i = 0; i < input.length(); i++) {
				char currentChar = input.charAt(i);
				if (currentChar == '{' || currentChar == '(' || currentChar == '[') {
					stack.push(input.charAt(i));
				} else if (currentChar == '}' || currentChar == ')' || currentChar == ']') {
					if (stack.isEmpty()) {
						return false;
					} else if (!isMatching(currentChar, stack.pop())) {
						return false;
					}
				} else {
					return false;
				}
			}
			if (stack.isEmpty())
				return true;
		} catch (Exception e) {
			return false;
		}

		return false;
	}

	private static boolean isMatching(char currentChar, char headChar) {
		if ((currentChar == ')' && headChar == '(') || (currentChar == '}' && headChar == '{')
				|| (currentChar == ']' && headChar == '[')) {
			return true;
		}
		return false;
	}
}
