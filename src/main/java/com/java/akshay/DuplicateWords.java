package com.java.akshay;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DuplicateWords {

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str = "This this is is is done by Saurabh Saurabh";
		String arr[]=str.split(" ");
		//List<String> s=Arrays.asList(arr);
	//	List<String> s2=s.stream().distinct().collect(Collectors.toList());
	//	System.out.println("Duplicate elements are:"+(s.size()-s2.size()));
		Map<String,Integer> hm=new HashMap<String, Integer>();
		int count=0;
		for(String s:arr) {
			if(hm.containsKey(s)) {
				hm.put(s, hm.get(s)+1);
			} else {
				hm.put(s, 1);
			}
		}
		
//		hm.forEach((k,v)->{
//			int count=0;
//			if(v>1) {
//				count=count+1;
//			}
//		});
		for(Entry<String,Integer> entry:hm.entrySet()) {
			if(entry.getValue()>1) {
				count++;
				System.out.println(entry.getKey());
			}
		}
		System.out.println(count);
		//Set<String> hashSet=new HashSet<String>();
//		for(String s3:arr) {
//			if(hashSet.contains(s3)) {
//				System.out.println(s3);
//			} else {
//				hashSet.add(s3);
//			}
//		}
		
		//Employee table
//		emp_id, emp_name,emp_salary
//		1 abc 123
//		1 abc 123
//		1 abc 345
//		1 abc 123
//		1 bcd 123
//		1 bcd 123
//		1 abc 123
//		output
//		1 abc 123 4
//		1 bcd 123 2
//		
//		select emp_id,emp_name,emp_salary,count(emp_id) from emp 
//		group by emp_id,emp_name,emp_salary having count(emp_id)>1;
	}

}
