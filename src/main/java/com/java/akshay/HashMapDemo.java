package com.java.akshay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class HashMapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Map<Integer,Integer> hashMap=new HashMap<Integer,Integer>();
		List<Integer> number=new ArrayList<Integer>();
		number.add(1);
		number.add(2);
		number.add(3);
		number.add(2);
		number.add(3);
		//iteration using lambda
		number.forEach(i->System.out.println(i));
		//removing duplicate elments from list using stram API
		System.out.println(number.stream().distinct().collect(Collectors.toList()));
		//int size=number.size();
		//System.out.println(size);
		System.out.println(number);
		for(int i=0;i<number.size();i++) {
			if(hashMap.containsKey(number.get(i))) {
				number.remove(i);
				i--;
				
			} else {
				hashMap.put(number.get(i), i);
				
			}
		}
		System.out.println(number);
		//iterating map using for each lambda
		hashMap.forEach((k,v)->System.out.println("key is:"+k+"  value is:"+v));
		//iterating map using for loop
		for(Map.Entry<Integer,Integer> map:hashMap.entrySet()) {
			System.out.println("key is:"+map.getKey()+"  value is:"+map.getValue());
			
		}
		
		
		//System.out.println(hashMap);
		

	}

}
