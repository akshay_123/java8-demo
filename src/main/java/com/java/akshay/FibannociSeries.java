package com.java.akshay;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FibannociSeries {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n1 = 0, n2 = 1;
		System.out.print(n1 + " " + n2);
		for (int i = 2; i < 10; i++) {
			int n3 = n1 + n2;
			System.out.print(" " + n3);
			n1 = n2;
			n2 = n3;

		}

		// USing Java8
		System.out.println();
		String fib = Stream.iterate(new int[] { 0, 1 }, t -> new int[] { t[1], t[0] + t[1] })
				.limit(10)
				.map(t -> t[0])
				.map(String::valueOf)
				.collect(Collectors.joining(" "));
		System.out.println(fib);

	}

}
