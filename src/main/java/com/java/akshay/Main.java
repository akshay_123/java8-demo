package com.java.akshay;

/* 
 * Save this in a file called Main.java and compile it. To test it 
 * create the file `input.txt` in the workspace / Working Directory
 * (For Eclipse/VisualStudio, by default this is the top level project folder)
 */

/* Do not add a package declaration */
import java.util.*;
import java.io.*;

/* DO NOT CHANGE ANYTHING ABOVE THIS LINE */
/* You may add any imports here, if you wish, but only from the 
   standard library */

/* Do not add a namespace declaration */
public class Main {
    public static Map<String,Integer> processData(ArrayList<String> array) {
    	Map<String,Integer> retVal = new HashMap<String,Integer>();
    	Map<String,Integer> retVal1= new HashMap<String,Integer>();
//    	22, Ravi Pawar, Aundh, 1603
//    	23, Suvarna Kale, Baner, 803
//    	27, Vinod Chavan, Aundh, 809
//    	29, Vasant Mahajan, Aundh, 617
//    	32, Aarti Patil, Baner, 351
//    	34, Swaran Bijur, Baner, 352
    	for(String s:array) {
    		//System.out.println(s);
    		String arr[]=s.split(",");
    		if(retVal.containsKey(arr[2])) {
    			int vote=retVal.get(arr[2]);
    			int curretVote=Integer.parseInt(arr[3].trim());
    			if(curretVote<vote) {
    				retVal.put(arr[2], curretVote);
    				retVal1.put(arr[2], Integer.parseInt(arr[0]));
    				
    			}
    			
    		}else {
    			int vote=Integer.parseInt(arr[3].trim());
				retVal.put(arr[2], vote);
				retVal1.put(arr[2], Integer.parseInt(arr[0]));
			}
    	}
        /* 
         * Modify this method to process `array` as indicated
         * in the question. At the end, return a Map containing
         * the appropriate values
         *
         * Please create appropriate classes, and use appropriate
         * data structures as necessary.
         *
         * Do not print anything in this method.
         *
         * Submit this entire program (not just this method)
         * as your answer
         */
        System.out.println(retVal1);
       return retVal1;
    }

    public static void main (String[] args) {
        ArrayList<String> inputData = new ArrayList<String>();
        String line;
        try {
            Scanner in = new Scanner(new BufferedReader(new FileReader("input.txt")));
            while(in.hasNextLine())
                inputData.add(in.nextLine());
            Map<String,Integer> retVal = processData(inputData);
            PrintWriter output = new PrintWriter(new BufferedWriter(new FileWriter("output.txt")));
            for(Map.Entry<String,Integer> e: retVal.entrySet())
                output.println(e.getKey() + ": " + e.getValue());
            output.close();
        } catch (IOException e) {
            System.out.println("IO error in input.txt or output.txt");
        }
    }
}
