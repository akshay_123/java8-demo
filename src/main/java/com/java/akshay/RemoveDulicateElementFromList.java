package com.java.akshay;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RemoveDulicateElementFromList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list=new ArrayList<String>();
		list.add("Akshay");
		list.add("Akshay");
		Set<String> hashSet=new HashSet<String>();

		for(String s:list) {
			if(hashSet.contains(s)) {
				list.remove(s);
			} else {
				hashSet.add(s);
			}
		}

	}	

}
