package com.java.akshay;

public class OverrideHshCodeEqualsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp1 = new Employee(1, "Akshay");
		Employee emp2 = new Employee(1, "Akshay");

		System.out.println("shallow comparison: " + (emp1 == emp2));

		// it will compare internal details
		System.out.println("Deep comparison: " + (emp1.equals(emp2)));

	}

}
