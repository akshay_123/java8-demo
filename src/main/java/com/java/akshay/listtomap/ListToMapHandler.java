package com.java.akshay.listtomap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListToMapHandler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<User> user = new ArrayList<User>();
		user.add(new User(1, "Akshay"));
		user.add(new User(2, "Ravi"));
		user.add(new User(1, "Roshan"));
		user.add(new User(4, "Neha"));

		Map<Integer, String> userMap = user.stream()
				.collect(Collectors.toMap(User::getId, User::getName, (oldValue, newValue) -> newValue));
		System.out.println(userMap);
	}

}
