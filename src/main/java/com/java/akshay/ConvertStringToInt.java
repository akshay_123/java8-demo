package com.java.akshay;

public class ConvertStringToInt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String input = "12345";

		if (input != null && !input.trim().isEmpty())
			System.out.println(getStringToIntValue(input));

	}

	private static int getStringToIntValue(String input) {
		if (input.length() == 1) {
			return input.charAt(0) - 48;
		}
		return 10 * getStringToIntValue(input.substring(0, input.length() - 1))
				+ (input.charAt(input.length() - 1) - 48);
	}

//	30
//	40+
}
