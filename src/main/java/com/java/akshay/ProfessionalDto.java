package com.java.akshay;

public class ProfessionalDto {
	private String data;
	private String hpr_work_details_id;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getHpr_work_details_id() {
		return hpr_work_details_id;
	}
	public void setHpr_work_details_id(String hpr_work_details_id) {
		this.hpr_work_details_id = hpr_work_details_id;
	}
	@Override
	public String toString() {
		return "ProfessionalDto [data=" + data + ", hpr_work_details_id=" + hpr_work_details_id + "]";
	}



}
