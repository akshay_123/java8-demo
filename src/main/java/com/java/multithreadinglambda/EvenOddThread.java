package com.java.multithreadinglambda;

public class EvenOddThread implements Runnable {

	private Object object;

	private static volatile int counter = 1;

	public EvenOddThread(Object object) {
		this.object = object;
	}

	@Override
	public void run() {
		while (counter < 10) {
			if (Thread.currentThread().getName().equals("even") && counter % 2 == 0) {
				synchronized (object) {
					System.out.println("Thread Name : " + Thread.currentThread().getName() + " value :" + counter);
					counter++;
					try {
						object.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			if (Thread.currentThread().getName().equals("odd") && counter % 2 != 0) {
				synchronized (object) {
					System.out.println("Thread Name : " + Thread.currentThread().getName() + " value :" + counter);
					counter++;
					object.notify();
				}
			}
		}
	}

}
