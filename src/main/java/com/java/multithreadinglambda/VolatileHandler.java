package com.java.multithreadinglambda;

public class VolatileHandler {

	private static volatile int counter = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * The volatile keyword in Java is used to indicate that a variable's value may
		 * be modified by multiple threads simultaneously. It ensures that the variable
		 * is always read from and written to the main memory, rather than from
		 * thread-specific caches, ensuring visibility across threads.
		 * 
		 * Imagine two people coordinating to assemble a puzzle, representing threads in
		 * Java. If one changes a piece (volatile variable), the volatile keyword
		 * ensures the other sees this change immediately. Misunderstanding the volatile
		 * keyword in Java can lead to confusing bugs and thread-safety issues.
		 * 
		 * The volatile keyword is applicable with both primitive types and objects.
		 * When declared volatile, a variable does not cache its value and always reads
		 * from the main memory. This is crucial, as it guarantees visibility and
		 * ordering, forbidding the compiler from reordering the code.
		 * 
		 * 
		 */

		Thread t1 = new Thread(() -> {
			int local_counter = counter;
			while (local_counter < 10) {
				if (local_counter != counter) {
					System.out.println("Thread [T1] is reading the data:"+(local_counter+1));
					local_counter = counter;
				}
			}

		});

		Thread t2 = new Thread(() -> {
			int local_counter = counter;
			while (local_counter < 10) {
				System.out.println("LOcal counter [T2] is incrementd the data: " + (local_counter + 1));
				counter = ++local_counter;

				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		});

		t1.start();
		t2.start();

	}

}
