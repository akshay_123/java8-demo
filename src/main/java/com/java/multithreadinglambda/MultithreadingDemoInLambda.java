package com.java.multithreadinglambda;

public class MultithreadingDemoInLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	/*Threading without lambda : It generate two .class file i.e. MyThread and MultithreadingDemoInLambda */
		//Using Thread Class
//		MyThread mt=new MyThread();
//		mt.start();
//		//using MyRunnable
//		MyRunnable mr=new MyRunnable();
//		Thread t=new Thread(mr);
//		t.start();
		
		/*Using Lambda Expression :It generate MultithreadingDemoInLambda.class file only.performance effective*/
		Runnable r= ()->{
			for(int i=0;i<10;i++) {
				System.out.println("Child in Lambda");
				
			}
		};
		Thread t= new Thread(r);
		t.start();
		
		
		for(int i=0;i<10;i++) {
			System.out.println("Inside Parent");
			
		}

	}

}
