/**
 * 
 */
package com.java.functionInterfaceAndLambda;

/**
 * @author 10696225
 *
 */
public class LambdaCallUsingFunInterface {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FunctionalInterfaceDemo fil= (a,b)->System.out.println(a+b);//Call using lambda function
		fil.add(10, 10);

	}

}
