package com.java.override;

public class Parent {

	public Object getParent() {
		return "From object";

	}

	public void test() {
		System.out.println("Calling from parent");

	}

}
