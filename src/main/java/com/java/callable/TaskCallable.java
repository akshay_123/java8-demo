package com.java.callable;

import java.util.concurrent.Callable;

public class TaskCallable implements Callable<String> {
	
	private String s;

	public TaskCallable(String s) {
		super();
		this.s = s;
	}

	@Override
	public String call() throws Exception {
		// TODO Auto-generated method stub
		
		return "Executing task:"+s;
	}

}
