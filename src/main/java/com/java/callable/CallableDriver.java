package com.java.callable;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class CallableDriver {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		ExecutorService service = Executors.newFixedThreadPool(2);
		TaskCallable tc = new TaskCallable("1");
		Future<String> future = service.submit(tc);
		String msg = future.get();
		System.out.println(msg);
		service.shutdown();

	}

}
