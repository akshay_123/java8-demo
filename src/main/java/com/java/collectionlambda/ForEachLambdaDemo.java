package com.java.collectionlambda;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ForEachLambdaDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> num=new ArrayList<Integer>();
		num.add(10);
		num.add(20);
		num.add(35);
		num.add(5);
		num.add(7);
		
		//looping using lambda expression
		num.forEach(System.out::println);
		num=num.stream().filter((i)-> i%2==0).collect(Collectors.toList());
		//num.stream().forEach(I->System.out.println(I));
		
		System.out.println(num);

	}

}
