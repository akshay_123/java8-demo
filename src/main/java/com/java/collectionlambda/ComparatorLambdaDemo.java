package com.java.collectionlambda;

import java.util.*;
import java.util.stream.Collector;

public class ComparatorLambdaDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> num=new ArrayList<Integer>();
		num.add(10);
		num.add(20);
		num.add(35);
		num.add(5);
		num.add(7);
		System.out.println(num);
		//sortung using lamda Aand collector
		Comparator<Integer> c=(i1,i2)-> (i1>i2?-1:i1<i2?1:0);
		Collections.sort(num,c);
		System.out.println(num);

		

	}

}
