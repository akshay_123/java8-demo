package com.java.executorservice;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExecutorService service= Executors.newSingleThreadExecutor();
		
		TaskRunnable tr=new TaskRunnable("1 task");
		TaskRunnable tr1=new TaskRunnable("2 task");
	//	list.add(tr);
	//	list.add(tr1);
		service.execute(tr);
		service.execute(tr1);
		
		for(int i=0;i<10;i++) {
			System.out.println("Executing main task");
		}
		service.shutdown();
		

		

	}

}
