package com.java.functionalinterface;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PredicateDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Predicate<Integer> greatenThan5 = i -> i > 5;
		System.out.println(greatenThan5.test(10));
		System.out.println(greatenThan5.test(5));

		List<String> names = new ArrayList<String>();
		names.add("Akshay");
		names.add("Raj");
		names.add("Ravi");
		Predicate<String> lengthgreatenThan5 = i -> i.length() > 5;
		// names.forEach(i->System.out.println(lengthgreatenThan5.test(i)));
		for (String s : names) {
			if (lengthgreatenThan5.test(s)) {
				System.out.println(s);
			}
		}

		/* predicate joining */
		// p1.and(p2)
		Predicate<Integer> divideBy2 = i -> i % 2 == 0;
		Predicate<Integer> greateThanOrEqual10 = i -> i >= 10;
		List<Integer> num = new ArrayList<Integer>();
		num.add(10);
		num.add(20);
		num.add(35);
		num.add(5);
		num.add(7);
		System.out.println("the greater than 10 Even number are");
		for (Integer i : num) {
			if (divideBy2.and(greateThanOrEqual10).test(i)) {
				System.out.println(i);
			}

		}
		System.out.println("the  odd number are");
		for (Integer i : num) {
			if (divideBy2.negate().test(i)) {
				System.out.println(i);
			}

		}

		// Predicate demo: check if number is divisible by 2
		Predicate<Integer> divBy2 = number -> number % 2 == 0;
		Predicate<Integer> greaterThan10 = number -> number > 10;
		System.out.println(divBy2.test(10));

		// Predicate Joining demo: check if number is divisible by 2 and greater than 10
		System.out.println(divBy2.and(greaterThan10).test(10));

		// Predicate Joining demo: check if number is divisible by 2 or greater than 10
		System.out.println(divBy2.or(greaterThan10).test(10));

		// Predicate Joining demo: negate
		System.out.println(divBy2.negate().test(10));

	}

}
