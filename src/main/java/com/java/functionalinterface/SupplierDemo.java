package com.java.functionalinterface;

import java.util.Date;
import java.util.function.Supplier;

public class SupplierDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Supplier<Date> dateSupplier=()-> new Date();
		System.out.println(dateSupplier.get());
		
		//6digit-OTP from 0-9 generation using Supplier
		//using a Math.Random(), which having min value 0 amd max value=9.99999999999999
		
		Supplier<String> otpSupplier=()->{
			String otp="";
			for(int i=0;i<6;i++) {
				otp=otp+(int)(Math.random()*10);
			}
			return otp;
			
		};
		System.out.println("OTP's");
		
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());
		System.out.println(otpSupplier.get());

	}

}
