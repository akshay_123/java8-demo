package com.java.functionalinterface;

import java.util.function.Function;

public class FunctionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// here input is String and output is Integer type
		Function<String, Integer> func = x -> x.length();

		Integer apply = func.apply("mkyong"); // 6

		System.out.println(apply);

		// Function chain
		Function<Integer, Integer> func2 = x -> x + 2;
		Function<Integer, Integer> func3 = x -> x * 2;

		// Returns a composed function that first applies first function to its input,
		// and then applies the after function to the result
		Integer result = func2.andThen(func3).apply(10);// 24

		// Returns a composed function that first applies the second function to its
		// input, and then applies 1st function to the result
		Integer result2 = func2.compose(func3).apply(10);// 22

		System.out.println(result);
		System.out.println(result2);

	}

}
