package com.java.stram.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortedMethod {

	public static void main(String[] args) {

		List<String> nameList = Arrays.asList("A", "BA", "CAB", "DABC");
		// sorting according to natural sorting order
		System.out.println("Natural sorting order: " + nameList.stream().sorted().collect(Collectors.toList()));

		// sorting according to reverse order
		System.out.println("Reverse sorting order: "
				+ nameList.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList()));

		// sorting according to custom max length
		System.out.println("Custom sorting order: " + nameList.stream()
				.sorted((s1, s2) -> s1.length() > s2.length() ? -1 : 0).collect(Collectors.toList()));

		// custom sorting
		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 28000f));
		productsList.add(new Product(4, "Sony Laptop", 28000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));

		System.out.println(productsList);

		// sorting accordind to price
		List<Product> priceSoetedProductList = productsList.stream().sorted((i1, i2) -> (i1.price > i2.price) ? -1 : 0)
				.collect(Collectors.toList());
		System.out.println(priceSoetedProductList);

	}

}
