package com.java.stram.api;

import java.util.ArrayList;
import java.util.List;

public class CountMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Product> productsList = new ArrayList<Product>();  
		  
        //Adding Products  
        productsList.add(new Product(1,"HP Laptop",25000f));  
        productsList.add(new Product(2,"Dell Laptop",30000f));  
        productsList.add(new Product(3,"Lenevo Laptop",28000f));  
        productsList.add(new Product(4,"Sony Laptop",28000f));  
        productsList.add(new Product(5,"Apple Laptop",90000f));
        
        //Calculate total count whose price is greate tahn 250000
        Long count=productsList.stream().filter(i->i.price>25000).count();
        System.out.println(count);

	}

}
