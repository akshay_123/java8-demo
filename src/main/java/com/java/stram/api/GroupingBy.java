package com.java.stram.api;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class GroupingBy {
	public static void main(String args[]) {
		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "HP Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 28000f));
		productsList.add(new Product(4, "Lenevo Laptop", 29000f));
		productsList.add(new Product(5, "Apple", 90000f));

		// Group By names
		Map<String, List<Product>> productGroupByNames = productsList.stream()
				.collect(Collectors.groupingBy(Product::getName));
		System.out.println(productGroupByNames);

		// Group Laptops by Name and count
		Map<String, Long> productGoupByNameAndCout = productsList.stream()
				.collect(Collectors.groupingBy(Product::getName, Collectors.counting()));
		System.out.println(productGoupByNameAndCout);

		// Group Laptops by Name and count null safe version
		Map<String, Long> productGoupByNameAndCout1 = productsList.stream().collect(Collectors
				.groupingBy(product -> product.getName() != null ? product.getName() : "NA", Collectors.counting()));
		System.out.println(productGoupByNameAndCout1);

		// find the highest price laptop details from each brand
		Map<String, Optional<Product>> highestPriceLaptopDetails = productsList.stream().collect(
				Collectors.groupingBy(Product::getName, Collectors.maxBy(Comparator.comparing(Product::getPrice))));
		System.out.println(highestPriceLaptopDetails);
		
		// find the highest price laptop details from each brand
		Map<String, Optional<Product>> highestPriceLaptopDetails1 = productsList.stream().collect(
				Collectors.groupingBy(Product::getName, Collectors.maxBy((p1,p2)->p1.price>p2.price?1:-1)));
		System.out.println(highestPriceLaptopDetails1);

		// Calculate Average price by laptop
		Map<String, Double> averagePricePerProduct = productsList.stream()
				.collect(Collectors.groupingBy(Product::getName, Collectors.averagingDouble(Product::getPrice)));
		System.out.println(averagePricePerProduct);

		// partiion products grtetr than 30000
		Map<Boolean, List<Product>> partionByPrice = productsList.stream()
				.collect(Collectors.partitioningBy(product -> product.getPrice() > 30000));
		System.out.println(partionByPrice);

		// Group List of prices per product
		Map<String, Set<Float>> ListOfPricePerProduct = productsList.stream().collect(
				Collectors.groupingBy(Product::getName, Collectors.mapping(Product::getPrice, Collectors.toSet())));
		System.out.println(ListOfPricePerProduct);
	}

}
