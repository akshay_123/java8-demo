package com.java.stram.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class ParalleStreamDemo {

	public static void main(String[] args) {
		List<String> tanleName = Arrays.asList("table1", "table2", "table3");
		System.out.println(new Date());
		//Multithreading replacement with parallel streamm
		List<Product> productList = tanleName.parallelStream().flatMap(table -> getProduct(table).stream())
				.collect(Collectors.toList());
		System.out.println(new Date());
		System.out.println(productList);

	}

	private static List<Product> getProduct(String tableName) {
		System.out.println("Processing statret for : " + tableName);
		try {
		Thread.sleep(1000);
		}catch (Exception e) {
			
		}
		List<Product> productsList = new ArrayList<>();
		Product p1 = new Product(1, tableName + ": HP Laptop", 25000f);
		productsList.add(p1);
		productsList.add(new Product(2, tableName + ": Dell Laptop", 30000f));
		productsList.add(new Product(3, tableName + ": Lenevo Laptop", 28000f));
		productsList.add(new Product(4, tableName + ": Sony Laptop", 28000f));
		System.out.println("Processing complted for : " + tableName);
		return productsList;
	}

}
