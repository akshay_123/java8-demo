package com.java.stram.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SkipAndLimitMatchMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 20000f));
		productsList.add(new Product(4, "Sony Laptop", 20000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));

		// skip first n elements
		productsList = productsList.stream().skip(2).collect(Collectors.toList());
		System.out.println(productsList);

		// Limit first n elements
		productsList = productsList.stream().limit(2).collect(Collectors.toList());
		System.out.println(productsList);

		/**
		 * Match
		 */

		List<Integer> numberList = Arrays.asList(10, 5, 7, 2, 1, 4);

		// Check if list contains all even number or not
		boolean isEvenList = numberList.stream().allMatch(n -> n % 2 == 0);
		System.out.println("isEvenList: " + isEvenList);

		List<String> nameList = Arrays.asList("Akshay", "Raj", "Ravi");

		// Check if any name contains A
		boolean containAInName = nameList.stream().anyMatch(name -> name.contains("A"));
		System.out.println("containAInName: " + containAInName);

		// Check if None name contains z
		boolean notContainZInName = nameList.stream().noneMatch(name -> name.contains("Z"));
		System.out.println("notContainZInName: " + notContainZInName);

	}

}
