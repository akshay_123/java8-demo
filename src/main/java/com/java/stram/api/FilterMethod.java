package com.java.stram.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FilterMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 28000f));
		productsList.add(new Product(4, "Sony Laptop", 28000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));

		// filter based on laptop name
		List<Product> dellProductsList = productsList.stream().filter(i -> i.name.equalsIgnoreCase("Dell Laptop"))
				.collect(Collectors.toList());
		System.out.println(dellProductsList);

		// filter based on laptop price
		Predicate<Product> pricePredicate = i -> i.price > 30000;
		List<Product> priceProductsList = productsList.stream().filter(pricePredicate).collect(Collectors.toList());
		System.out.println(priceProductsList);

	}

}
