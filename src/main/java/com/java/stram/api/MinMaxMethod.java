package com.java.stram.api;

import java.util.ArrayList;
import java.util.List;

public class MinMaxMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> number = new ArrayList<Integer>();
		number.add(10);
		number.add(5);
		number.add(15);
		number.add(7);
		number.add(19);
		number.add(6);

		// Find min element based on ascending order
		int minNumber = number.stream().min((i1, i2) -> i1.compareTo(i2)).get();
		System.out.println(minNumber);

		// Find max element based on ascending order
		int maxNumber = number.stream().max((i1, i2) -> i1.compareTo(i2)).get();
		System.out.println(maxNumber);

	}

}
