package com.java.stram.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DistinctMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> productsList = new ArrayList<Product>();
		Product p1 = new Product(1, "HP Laptop", 25000f);
		productsList.add(p1);
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 28000f));
		productsList.add(new Product(4, "Sony Laptop", 28000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));
		productsList.add(p1);
		
		// print unique list
		System.out.println("Distinct List : " + productsList.stream().distinct().collect(Collectors.toList()));

		// List of distinct price
		List<Float> distinctPrice = productsList.stream().map(i -> i.price).distinct().collect((Collectors.toList()));
		System.out.println(distinctPrice);

	}

}
