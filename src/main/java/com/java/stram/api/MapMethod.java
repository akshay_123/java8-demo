package com.java.stram.api;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 28000f));
		productsList.add(new Product(4, "Sony Laptop", 28000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));

		// increase the price by 100rs and print the object
		Function<Product, Product> prodcutFunc = product -> {
			product.setPrice(product.getPrice() + 1000);
			return product;
		};
		productsList.stream().map(prodcutFunc).collect(Collectors.toList()).forEach(System.out::println);

		// Get the list of laptop names from the list
		List<String> lNamesList = productsList.stream().map(Product::getName).collect(Collectors.toList());
		System.out.println(lNamesList);

	}

}
