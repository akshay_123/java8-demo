package com.java.stram.api;

import java.util.List;

public class Developer {
	public int id;
	public String name;
	public List<String> numbers;
	public Developer(int id, String name, List<String> numbers) {
		super();
		this.id = id;
		this.name = name;
		this.numbers = numbers;
	}
	@Override
	public String toString() {
		return "Developer [id=" + id + ", name=" + name + ", numbers=" + numbers + "]";
	}
	
	

}
