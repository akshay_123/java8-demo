package com.java.stram.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FlatMapMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Developer> developers = Stream.of(new Developer(1, "Akshay", Arrays.asList("123", "456")),
				new Developer(2, "Ravi", Arrays.asList("321", "654")),
				new Developer(3, "Shyam", Arrays.asList("135", "690"))).collect(Collectors.toList());

		// Data transformatiom
		// one to one maaping because developer=> one email
		List<String> names = developers.stream().map(i -> i.name).collect(Collectors.toList());
		System.out.println(names);

		// in this case data flattering is needdes because stream containg stream of
		// stream
		List<List<String>> phone = developers.stream().map(i -> i.numbers).collect(Collectors.toList());
		System.out.println(phone);

		// Data flattering
		// one to many developer => multiple phone
		List<String> phones = developers.stream().flatMap(i -> i.numbers.stream()).collect(Collectors.toList());
		System.out.println(phones);

		String commaSeperatedPhones = developers.stream().flatMap(developer -> developer.numbers.stream())
				.collect(Collectors.joining(","));
		System.out.println("commaSeperatedPhones: " + commaSeperatedPhones);

		// flattering the list of devs
		List<Developer> developers1 = Stream.of(new Developer(4, "Akshay", Arrays.asList("123", "456")),
				new Developer(5, "Ravi", Arrays.asList("321", "654")),
				new Developer(6, "Shyam", Arrays.asList("135", "690"))).collect(Collectors.toList());

		List<List<Developer>> developerUnflatterList = new ArrayList<>();
		developerUnflatterList.add(developers);
		developerUnflatterList.add(developers1);
		System.out.println("Count: " + developerUnflatterList.stream().count());
		System.out.println("Before Falttering : " + developerUnflatterList);
		List<Developer> developerflatterList = developerUnflatterList.stream().flatMap(devList -> devList.stream())
				.collect(Collectors.toList());
		System.out.println("After Falttering : " + developerflatterList);

	}

}
