package com.java.stram.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReduceMethode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> numberList = Arrays.asList(10, 5, 7, 2, 1, 4);

		// Find the sume of all the elements
		int sum = numberList.stream().reduce(0, (num1, num2) -> num1 + num2);
		System.out.println(sum);

		// alternative way
		sum = numberList.stream().reduce(0, Integer::sum);
		System.out.println(sum);

		// find the max and min element from the list
		int min = numberList.stream().reduce(Integer.MAX_VALUE, (num1, num2) -> num1 < num2 ? num1 : num2);
		System.out.println(min);

		// alternative way
		int min1 = numberList.stream().reduce(Integer.MAX_VALUE, Integer::min);
		System.out.println(min1);

		// find the max and min element from the list
		int max = numberList.stream().reduce(Integer.MIN_VALUE, (num1, num2) -> num1 > num2 ? num1 : num2);
		System.out.println(max);

		// alternative way
		int max1 = numberList.stream().reduce(Integer.MIN_VALUE, Integer::max);
		System.out.println(max1);

		/**
		 * Parallelising reduce operation
		 */

		sum = numberList.parallelStream().reduce(0, (num1, num2) -> num1 + num2, Integer::sum);

		// Alternative way: when reduction type is the same as stream element type then
		// accumulator will work
		// combiner as well in case of parallel stream
		sum = numberList.parallelStream().reduce(0, (num1, num2) -> num1 + num2);
		System.out.println(sum);

		/**
		 * Use of combiner
		 */
		List<Product> productsList = new ArrayList<Product>();

		// Adding Products
		productsList.add(new Product(1, "HP Laptop", 25000f));
		productsList.add(new Product(2, "Dell Laptop", 30000f));
		productsList.add(new Product(3, "Lenevo Laptop", 20000f));
		productsList.add(new Product(4, "Sony Laptop", 20000f));
		productsList.add(new Product(5, "Apple Laptop", 90000f));

		// the types of the accumulator arguments and the types of its implementation
		// don't match, so we need to use a combiner.because input type is product and
		// reduction type is double
		double doublePrice = productsList.stream().reduce(0.0, (num1, product) -> num1 + product.price, Double::sum);
		System.out.println(doublePrice);

		List<String> nameList = Arrays.asList("Akshay", "Raj", "Ravi");

		int numerOfCharaters = nameList.stream().reduce(0, (s1, s2) -> s1 + s2.length(), Integer::sum);
		System.out.println(numerOfCharaters);

		String joinBySpace = nameList.stream().reduce("", String::concat);
		System.out.println(joinBySpace);

	}

}
