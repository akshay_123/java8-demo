package com.demo.interview;

public class VarArgsDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		varArg("10","10","30");

	}

	public static void varArg(String s, String... x) {
		System.out.println(s);
		for (int i = 0; i < x.length; i++) {
			System.out.println(x[i]);
		}

	}

}
