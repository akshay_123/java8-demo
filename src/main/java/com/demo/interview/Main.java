package com.demo.interview;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
	public static void main(String []args) {
		System.out.println(Child.a);
		
		Public p=new Public("a1","a2");
		Public p1=new Public("b1","b2");
		Public p2=new Public("c1","c2");
		List<Public> list= new ArrayList<Public>();
		list.add(p);
		list.add(p1);
		list.add(p2);
		
		List<String> list1 = list.stream().map(Public::getVal1).collect(Collectors.toList());
		System.out.println(list1);
		
	}

}
