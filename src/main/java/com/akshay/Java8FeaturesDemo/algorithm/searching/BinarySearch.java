package com.akshay.Java8FeaturesDemo.algorithm.searching;

public class BinarySearch {

	public static void main(String args[]) {
		int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int x = 10;

		int index = binarySearch(arr, x, arr.length);
		if (index >= 0)
			System.out.println("Element: " + x + " is present at position: " + index);

	}

	private static int binarySearch(int[] arr, int x, int length) {
		int start = 0;
		int end = length - 1;
		while (start < end) {
			int mid = (start + end) / 2;
			if (arr[mid] == x) {
				return mid;
			} else if (x < arr[mid]) {
				end = mid - 1;
			} else {
				start = mid + 1;
			}

		}
		return -1;
	}

}
