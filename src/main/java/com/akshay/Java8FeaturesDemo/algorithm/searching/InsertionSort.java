package com.akshay.Java8FeaturesDemo.algorithm.searching;

public class InsertionSort {

	public static void main(String[] args) {
		int arr[] = { 4, 6, 9, 3, 5, 8, 1 };
		insertionSort(arr);

		for (int element : arr) {
			System.out.println(element);
		}

	}

	private static void insertionSort(int[] arr) {
		for (int i = 1; i < arr.length; i++) {
			int key = arr[i];
			int j = i;

			while (j > 0 && arr[j - 1] > key) {
				arr[j] = arr[j - 1];
				arr[j - 1] = key;
				j--;
			}
		}
	}

}
