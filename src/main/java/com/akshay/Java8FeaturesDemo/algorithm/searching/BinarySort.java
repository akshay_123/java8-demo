package com.akshay.Java8FeaturesDemo.algorithm.searching;

public class BinarySort {

	public static void main(String[] args) {
		int arr[] = { 4, 6, 9, 3, 5, 8, 1 };
		bubbleSort(arr);

		for (int element : arr) {
			System.out.println(element);
		}

	}

	private static void bubbleSort(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr.length - i - 1; j++) {
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j + 1];
					arr[j + 1] = arr[j];
					arr[j] = temp;
				}
			}

		}

	}

}
