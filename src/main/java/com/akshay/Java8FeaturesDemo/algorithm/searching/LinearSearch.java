package com.akshay.Java8FeaturesDemo.algorithm.searching;

public class LinearSearch {

	public static void main(String args[]) {
		int arr[] = { 4, 5, 2, 1, 9, 8, 3 };
		int x = 10;

		int index = searchLinearly(arr, x);
		if (index >= 0)
			System.out.println("Element: " + x + " is present at position: " + index);

	}

	private static int searchLinearly(int[] arr, int x) {

		for (int i = 0; i < arr.length; i++) {
			if (x == arr[i])
				return i;
		}

		return -1;
	}

}
