package com.akshay.Java8FeaturesDemo.algorithm.searching;

public class SelectionSort {

	public static void main(String[] args) {
		int arr[] = { 4, 6, 9, 3, 5, 8, 1 };
		seletionSort(arr);

		for (int element : arr) {
			System.out.println(element);
		}

	}

	private static void seletionSort(int[] arr) {
		for (int i = 0; i < arr.length - 1; i++) {
			int minElement = arr[i];
			int minIndex = i;
			for (int j = i + 1; j < arr.length; j++) {
				if (arr[j] < minElement) {
					minElement = arr[j];
					minIndex = j;
				}

			}
			// swapping
			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}

	}

}
