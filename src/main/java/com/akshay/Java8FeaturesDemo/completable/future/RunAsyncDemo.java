package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RunAsyncDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// saveEmployee();
		saveEmployeeCustomExecutor();

	}

	public static Void saveEmployee() throws InterruptedException, ExecutionException {
		CompletableFuture<Void> runAsyncFuture = CompletableFuture.runAsync(() -> {
			// Get Employee
			List<Employee> employees = EmployeeDatabase.getEmployee();
			// save employee
			System.out.println("Thread started from pool: " + Thread.currentThread().getName());
			System.out.println(employees.size());
		});
		return runAsyncFuture.get();
	}

	public static Void saveEmployeeCustomExecutor() throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		CompletableFuture<Void> runAsyncFuture = CompletableFuture.runAsync(() -> {
			// Get Employee
			List<Employee> employees = EmployeeDatabase.getEmployee();
			
			// save employee
			System.out.println("Thread started from pool: " + Thread.currentThread().getName());
			System.out.println(employees.size());
		}, executor);
		return runAsyncFuture.get();
	}

}
