package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.Arrays;
import java.util.List;

public class EmployeeDatabase {

	public static List<Employee> getEmployee(){
		return Arrays.asList(
				new Employee(1, "name1", "1@gmail.com", "TRUE", "YES"),
				new Employee(2, "name2", "2@gmail.com", "TRUE", "YES"),
				new Employee(3, "name3", "3@gmail.com", "TRUE", "YES"),
				new Employee(4, "name4", "4@gmail.com", "TRUE", "YES"),
				new Employee(5, "name5", "5@gmail.com", "TRUE", "YES"),
				new Employee(6, "name6", "6@gmail.com", "TRUE", "YES"),
				new Employee(7, "name7", "7@gmail.com", "TRUE", "YES"),
				new Employee(8, "name8", "8@gmail.com", "TRUE", "YES"),
				new Employee(9, "name9", "9@gmail.com", "TRUE", "YES"),
				new Employee(10,"name10","10@gmail.com","TRUE", "YES"),
				new Employee(11,"name11","11@gmail.com","TRUE", "NO"),
				new Employee(12,"name12","12@gmail.com","NO", "NO"));
	}

}
