package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ThenApplyConsumeRunAsyncDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		remindEmployee();

	}

	private static Void remindEmployee() throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		CompletableFuture<Void> future = CompletableFuture.supplyAsync(() -> {
			System.out.println("Fetching employee started from pool by Thread: " + Thread.currentThread().getName());
			return EmployeeDatabase.getEmployee();
		}, executor).thenApplyAsync(employees -> {
			System.out.println("Filtering new joinee started from pool by Thread: " + Thread.currentThread().getName());
			return employees.stream().filter(employee -> "TRUE".equals(employee.getNewJoiner()))
					.collect(Collectors.toList());
		}, executor).thenApplyAsync(employees -> {
			System.out.println("Get emails of training Pending  employee started from pool by Thread: "
					+ Thread.currentThread().getName());
			return employees.stream().filter(employee -> "YES".equals(employee.getLearningPending()))
					.map(employee -> employee.getEmail()).collect(Collectors.toList());
		}, executor).thenAcceptAsync(emails -> {
			System.out.println("send emails to training Pending  employee started from pool by Thread: "
					+ Thread.currentThread().getName());
			emails.forEach(email -> System.out.println("Email sent on emailid: " + email));
		}, executor);
		Void v = future.get();
		executor.shutdown();
		return v;
	}

}
