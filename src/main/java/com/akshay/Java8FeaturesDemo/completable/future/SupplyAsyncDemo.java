package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SupplyAsyncDemo {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		List<Employee> employee = getEmployeeCustomExecutor();
		System.out.println(employee.size());

	}

	public static List<Employee> getEmployee() throws InterruptedException, ExecutionException {
		CompletableFuture<List<Employee>> runAsyncFuture = CompletableFuture.supplyAsync(() -> {
			System.out.println("Thread started from pool: " + Thread.currentThread().getName());

			// Get Employee
			return EmployeeDatabase.getEmployee();
		});
		return runAsyncFuture.get();
	}

	public static List<Employee> getEmployeeCustomExecutor() throws InterruptedException, ExecutionException {
		ExecutorService executor = Executors.newFixedThreadPool(5);
		CompletableFuture<List<Employee>> runAsyncFuture = CompletableFuture.supplyAsync(() -> {
			System.out.println("Thread started from pool: " + Thread.currentThread().getName());

			// Get Employee
			return EmployeeDatabase.getEmployee();
		}, executor);
		return runAsyncFuture.get();
	}

}
