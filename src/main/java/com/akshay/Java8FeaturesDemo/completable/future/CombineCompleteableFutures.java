package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.concurrent.CompletableFuture;

public class CombineCompleteableFutures {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Future 1
		CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> {
			return 10;
		});

		// Future 2
		CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> {
			return 20;
		});

		// Future 3
		CompletableFuture<Integer> future3 = CompletableFuture.supplyAsync(() -> {
			return 30;
		});

		// Combine All three futures
		CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(future1, future2, future3);

		combinedFuture.thenRun(() -> {
			int result1 = future1.join();
			int result2 = future2.join();
			int result3 = future3.join();
			System.out.println(result1 + " , " + result2 + " , " + result3);
		});

	}

}
