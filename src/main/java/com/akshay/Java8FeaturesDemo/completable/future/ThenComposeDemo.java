package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.concurrent.CompletableFuture;

public class ThenComposeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> "Hello");
		/**
		 * thenComposeAsync() is a method in CompletableFuture that allows you to chain
		 * multiple asynchronous tasks together in a non-blocking way. This method is
		 * used when you have one CompletableFuture object that returns another
		 * CompletableFuture object as its result, and you want to execute the second
		 * task after the first one has completed.
		 * 
		 * The thenComposeAsync() method takes a Function object as its argument, which
		 * takes the result of the first CompletableFuture object as its input and
		 * returns another CompletableFuture object as its result. The second task is
		 * executed when the first one completes, and its result is passed to the next
		 * stage of the pipeline.
		 */
		CompletableFuture<String> future2 = future1
				.thenComposeAsync(s -> CompletableFuture.supplyAsync(() -> s + " World"));
		future2.thenAcceptAsync(s -> System.out.println(s));

	}

}
