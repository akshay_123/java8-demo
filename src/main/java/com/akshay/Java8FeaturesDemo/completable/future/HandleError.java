package com.akshay.Java8FeaturesDemo.completable.future;

import java.util.concurrent.CompletableFuture;

public class HandleError {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> {
			int result = 10 / 0; // Causes an ArithmeticException
			return result;
		});

		future.exceptionally(ex -> {
			System.out.println("Exception occurred: " + ex.getMessage());
			return 0; // Default value to return if there's an exception
		}).thenAccept(result -> {
			System.out.println("Result: " + result);
		});

	}

}
