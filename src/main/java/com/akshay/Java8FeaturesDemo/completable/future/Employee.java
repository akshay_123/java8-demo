package com.akshay.Java8FeaturesDemo.completable.future;

public class Employee {
	private int employeeId;
	private String name;
	private String email;
	private String newJoiner;
	private String learningPending;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNewJoiner() {
		return newJoiner;
	}

	public void setNewJoiner(String newJoiner) {
		this.newJoiner = newJoiner;
	}

	public String getLearningPending() {
		return learningPending;
	}

	public void setLearningPending(String learningPending) {
		this.learningPending = learningPending;
	}

	public Employee(int employeeId, String name, String email, String newJoiner, String learningPending) {
		super();
		this.employeeId = employeeId;
		this.name = name;
		this.email = email;
		this.newJoiner = newJoiner;
		this.learningPending = learningPending;
	}

	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", name=" + name + ", email=" + email + ", newJoiner=" + newJoiner
				+ ", learningPending=" + learningPending + "]";
	}

}
