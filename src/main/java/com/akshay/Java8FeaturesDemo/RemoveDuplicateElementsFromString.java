package com.akshay.Java8FeaturesDemo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveDuplicateElementsFromString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s1="Akshay chaugule Akshay";
		String arr[]=s1.split(" ");
		List<String> lstring=Arrays.asList(arr);
		System.out.println(lstring.stream().distinct().collect(Collectors.toList()));
		
	}

}
