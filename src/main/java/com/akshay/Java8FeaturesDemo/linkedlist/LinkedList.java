package com.akshay.Java8FeaturesDemo.linkedlist;

public class LinkedList {
	static class Node {
		int data;
		Node next;

		public Node(int data) {
			this.data = data;
			this.next = null;
		}
	}

	Node head = null;
	Node tail = null;

	public void add(int data) {
		Node newNode = new Node(data);

		if (head == null) {
			head = newNode;
			tail = newNode;
		} else {
			tail.next = newNode;
			tail = newNode;
		}
	}

	public void printList(LinkedList ll) {
		if (ll.head == null) {
			System.out.println("List is empty");
			return;
		}
		Node temp = ll.head;
		System.out.print("LinkedList elements: ");
		while (temp != null) {
			System.out.print(temp.data + " ");
			temp = temp.next;
		}
		System.out.println();
	}

	public boolean remove(int element) {
		if (this.head == null) {
			System.out.println("List is empty");
			return false;
		}

		// check if elemet present in head
		if (this.head.data == element) {
			head = head.next;
			return true;
		}

		// check if element present in some other location
		Node currentHead = this.head.next;
		Node previous = this.head;
		while (currentHead != null) {
			if (currentHead.data == element) {
				previous.next = currentHead.next;
				return true;
			}
			previous = currentHead;
			currentHead = currentHead.next;
		}
		return false;
	}

	public boolean removeByIndex(int index) {
		if (index < 0)
			throw new NegativeArraySizeException();
		if (this.head == null) {
			System.out.println("List is empty");
			return false;
		}

		// check if elemet present in head
		if (index == 0) {
			this.head = this.head.next;
			return true;
		}

		// check if element present in some other location
		Node currentHead = this.head.next;
		Node previous = this.head;
		int counter = 1;
		while (currentHead != null && counter <= index) {
			if (counter == index) {
				previous.next = currentHead.next;
				return true;
			}
			previous = currentHead;
			currentHead = currentHead.next;
			counter++;
		}

		if (index >= counter)
			throw new IndexOutOfBoundsException();
		return false;
	}

}
