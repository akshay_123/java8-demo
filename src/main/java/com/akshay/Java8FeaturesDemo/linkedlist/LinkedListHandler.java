package com.akshay.Java8FeaturesDemo.linkedlist;

public class LinkedListHandler {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList numberList = new LinkedList();
		numberList.add(10);
		numberList.add(20);
		numberList.add(30);
		numberList.add(40);
		numberList.add(50);

		System.out.println("Before Delete");
		numberList.printList(numberList);

		// numberList.remove(50);
		numberList.removeByIndex(-5);

		System.out.println("After Delete");
		numberList.printList(numberList);

	}

}
