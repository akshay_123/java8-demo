package com.akshay.Java8FeaturesDemo;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StringStream {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String input = "akshay";
		Map<Character, Long> charOccurrences = input.toLowerCase().chars().mapToObj(c -> (char) c) // Convert int to
																									// char
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

		charOccurrences.forEach((key, value) -> System.out.println("Key:" + key + " value: " + value));
		
		
	}

}
