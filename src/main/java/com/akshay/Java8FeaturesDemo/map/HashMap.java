package com.akshay.Java8FeaturesDemo.map;

public class HashMap<K, V> {
	private static final int DEFAULT_SIZE = 16;
	Entry<K, V>[] buckets;
	
	static class Entry<K, V> {
		K key;
		V value;
		Entry<K, V> next;

		public Entry(K key, V value) {
			super();
			this.key = key;
			this.value = value;
		}

	}
	public HashMap() {
		this(DEFAULT_SIZE);
	}

	public HashMap(int size) {
		buckets = new Entry[size];
	}

	public void put(K key, V value) {

	}

	public int index(K key) {
		return Math.abs(key.hashCode() % DEFAULT_SIZE);
	}

	public V get(K key) {
		V value = null;
		return value;
	}

	

}
